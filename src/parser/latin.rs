//! `UnicodeParser` implementation for Latin text..
use crate::{Translator, UnicodeParser};

/// `UnicodeParser` implementation which can parse Latin text.
/// Calling `parse` on anything other then Latin text is undefined behavior.
pub struct LatinParser<T: Translator> {
    cur_index: usize,
    text_chars: Vec<char>,
    bcharmap: T,
}

impl<T: Translator> LatinParser<T> {
    pub fn new(bcharmap: T) -> Self {
        Self {
            cur_index: 0,
            text_chars: vec![],
            bcharmap,
        }
    }

    fn decode_char(&mut self, c: char) -> Vec<u8> {
        match c {
            ch if ch.is_numeric() => self.numeric(),
            ch if ch.is_uppercase() => self.uppercase(),
            ch if ch != '\n' => {
                self.cur_index += 1;
                vec![self.to_braille(c)]
            }
            _ => {
                self.cur_index += 1;
                vec![]
            }
        }
    }

    fn numeric(&mut self) -> Vec<u8> {
        let mut res = vec![self.bcharmap.to_braille(&format!("num"))];

        while let Some(c) = self.current() {
            if c.is_numeric() {
                res.push(self.to_braille(c));
            } else if c == '.' || c == ',' {
                if let Some(nc) = self.next_char() {
                    if nc.is_numeric() {
                        res.push(self.to_braille(c));
                    }
                }
            } else {
                break;
            }
            self.cur_index += 1;
        }

        res
    }

    fn uppercase(&mut self) -> Vec<u8> {
        let mut res = vec![self.bcharmap.to_braille(&format!("upp"))];

        while let Some(c) = self.current() {
            if c.is_uppercase() {
                res.push(self.to_braille(c));
            } else if res.len() > 2 {
                res.insert(0, self.bcharmap.to_braille(&format!("upp")));
                break;
            } else {
                break;
            }
            self.cur_index += 1;
        }

        res
    }

    fn next_char(&self) -> Option<char> {
        self.get_char(self.cur_index + 1)
    }

    fn current(&self) -> Option<char> {
        self.get_char(self.cur_index)
    }

    fn get_char(&self, index: usize) -> Option<char> {
        self.text_chars.get(index).cloned()
    }

    fn to_braille(&self, c: char) -> u8 {
        self.bcharmap.to_braille(&c.to_string().to_lowercase())
    }
}

impl<T: Translator> UnicodeParser for LatinParser<T> {
    fn parse(&mut self, text: String) -> Vec<u8> {
        self.text_chars = text.chars().collect();
        self.cur_index = 0;
        let mut res = vec![];

        while let Some(c) = self.current() {
            res.append(&mut self.decode_char(c));
        }

        res
    }
}
