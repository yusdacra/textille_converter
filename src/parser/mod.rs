//! Parser traits and implementations.
pub mod latin;

pub use latin::LatinParser;

/// Implements methods to parse and convert Unicode text to Braille.
pub trait UnicodeParser {
    /// Parses given Unicode text and converts to Braille.
    fn parse(&mut self, text: String) -> Vec<u8>;
}

/// Implements methods to parse and convert Braille to Unicode text.
pub trait BrailleParser {
    /// Parses given Braille and converts to Unicode text.
    fn parse(&mut self, braille: Vec<u8>) -> String;
}
