use i2cdev::{core::*, linux::{LinuxI2CError, LinuxI2CDevice}};
use ron::de::from_str;
use serde::Deserialize;
use std::process::Command;
use std::{
    io::{Read, Write},
    time::Duration,
    
};
use textille_converter::{parser::LatinParser, translator::BrailleCharMap, UnicodeParser};

const RESULT_PATH: &str = "textille_converter_result";
const IMAGE_READ_PATH: &str = "/tmp/textille_converter_read";

const MAX_CHAR_PER_LINE: usize = 28;

fn main() {
    // Read config from config file
    let config: Config =
        ron::de::from_str(&std::fs::read_to_string("./conf.ron").unwrap()).unwrap();
    println!("Configuration: {:?}", config);

    let mut arduino_i2c =
        LinuxI2CDevice::new(&config.arduino_i2c_path, config.arduino_i2c_addr).unwrap();
    println!("Arduino port successfully configured!");

    let mut bluetooth_port = serialport::open(&config.bluetooth_serial_path).unwrap();
    println!("Bluetooth port successfully configured!");

    // Read character map from language file
    let bcharmap: BrailleCharMap =
        from_str(&std::fs::read_to_string(format!("./lang/{}.ron", &config.language)).unwrap())
            .unwrap();

    println!(
        "Braille character map that will be used to convert text: {:?}",
        bcharmap
    );

    // Create parser with specified charmap, convert and parse the text
    let mut parser = LatinParser::new(bcharmap);

    let mut ble_buf: Vec<u8> = Vec::with_capacity(255);

    loop {
       bluetooth_port.read(ble_buf.as_mut_slice()).unwrap();

        if !ble_buf.is_empty() {
            let as_string = ble_buf.iter().map(|u| *u as char).collect::<String>();

            if as_string.contains("wc") {
                bluetooth_port.write("ac".as_bytes()).unwrap();
                bluetooth_port.flush().unwrap();
            } else if as_string.contains("rc") {
                break;
            }
        }
    }

    ble_buf.clear();

    let mut getting_image = false;
    let mut image_buf: Vec<u8> = Vec::with_capacity(1024 * 100);
    let mut has_new_image = false;
    let mut parsed_text = String::new();

    loop {
        if has_new_image && parsed_text.is_empty() {
            parsed_text = parser
                .parse(ocr_image(IMAGE_READ_PATH, &config).unwrap())
                .into_iter()
                .map(|c| c as char)
                .collect();
            has_new_image = false;
            continue;
        } else if !parsed_text.is_empty() {
            instruct_arduino(&mut arduino_i2c, &parsed_text, &mut parser).unwrap();
            parsed_text.clear();
        }

        bluetooth_port.read(ble_buf.as_mut_slice()).unwrap();

        if !ble_buf.is_empty() {
            if getting_image {
                image_buf.append(&mut ble_buf);
                if let Ok(format) = image::guess_format(image_buf.as_slice()) {
                    if let Ok(i) = image::load_from_memory_with_format(image_buf.as_slice(), format)
                    {
                        if i.save_with_format(IMAGE_READ_PATH, format).is_ok() {
                            has_new_image = true;
                            getting_image = false;
                            image_buf.clear();
                        }
                    }
                }
            } else {
                let as_string = ble_buf.iter().map(|u| *u as char).collect::<String>();

                if as_string.contains("dc") {
                    break;
                } else if as_string.contains("si") {
                    getting_image = true;
                }
            }
        }
    }
}

fn wait_for_i2c(port: &mut LinuxI2CDevice, data: &[u8]) -> Result<(), LinuxI2CError> {
    let mut buf: Vec<u8> = Vec::with_capacity(data.len());

    loop {
        port.read(buf.as_mut_slice())?;

        if buf.as_slice() == data {
            break Ok(());
        }
    }
}

fn instruct_arduino(
    ap: &mut LinuxI2CDevice,
    text: &String,
    parser: &mut LatinParser<BrailleCharMap>,
) -> Result<(), LinuxI2CError> {
    // Send the arduino that we want to start printing
    //
    // When arduino receives this it will prepare the printer
    // and then send "ready" instruction
    ap.write(&[4, 0])?;
    wait_for_i2c(ap, &[4, 0])?;

    // Calculate and send "deltas" to arduino
    for line in format_text(text).lines() {
        for deltas in calculate_move_deltas(parser.parse(line.to_owned())).iter() {
            ap.write(deltas.as_slice())?;

            // Send instruction to print line
            ap.write(&[3, 0])?;
            wait_for_i2c(ap, &[3, 0])?;
        }
    }

    Ok(())
}

fn calculate_move_deltas(braille_chars: Vec<u8>) -> [Vec<u8>; 4] {
    fn check_if_bit_set(c: u8, index: u8) -> bool {
        ((c >> index) & 1) > 0
    }

    let mut result = [vec![], vec![], vec![], vec![]];

    for (index, ch) in braille_chars.into_iter().enumerate() {
        if check_if_bit_set(ch, 0) {
            result[3].push(1);
            result[3].push(index as u8 * 2);
        }
        if check_if_bit_set(ch, 3) {
            result[3].push(1);
            result[3].push((index as u8 * 2) + 1);
        }
        if check_if_bit_set(ch, 1) {
            result[2].push(1);
            result[2].push(index as u8 * 2);
        }
        if check_if_bit_set(ch, 4) {
            result[2].push(1);
            result[2].push((index as u8 * 2) + 1);
        }
        if check_if_bit_set(ch, 2) {
            result[1].push(1);
            result[1].push(index as u8 * 2);
        }
        if check_if_bit_set(ch, 5) {
            result[1].push(1);
            result[1].push((index as u8 * 2) + 1);
        }
        if check_if_bit_set(ch, 6) {
            result[0].push(1);
            result[0].push(index as u8 * 2);
        }
        if check_if_bit_set(ch, 7) {
            result[0].push(1);
            result[0].push((index as u8 * 2) + 1);
        }
    }
    result
}

fn format_text(text: &String) -> String {
    let mut result = String::with_capacity(text.capacity());

    let mut index = 0;

    while index < text.len() {
        result.push_str(
            text[index..index + MAX_CHAR_PER_LINE]
                .chars()
                .filter(|c| c != &'\n' || c != &'\r')
                .collect::<String>()
                .as_str(),
        );
        result.push('\n');
        index += MAX_CHAR_PER_LINE;
    }
    result.push_str(&text[index - MAX_CHAR_PER_LINE..text.len()]);
    result.trim().to_owned()
}

fn ocr_image(photo_path: &str, config: &Config) -> std::io::Result<String> {
    // Configure Tesseract for usage
    let mut tesseract_command = Command::new("tesseract");
    // Process the photo and get the text
    tesseract_command
        .env(
            "TESSDATA_PREFIX",
            "/home/patriot/Belgeler/dev/rust/textille_converter/",
        )
        .arg(photo_path)
        .arg(RESULT_PATH)
        .args(&["-l", &config.language])
        .spawn()
        .unwrap();
    std::fs::read_to_string(format!("{}.txt", RESULT_PATH))
}

#[derive(Deserialize, Debug)]
struct Config {
    language: String,
    tess_data_path: String,
    bluetooth_serial_path: String,
    arduino_i2c_path: String,
    arduino_i2c_addr: u16,
}
