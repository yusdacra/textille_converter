//! Simple `Translator` implementation.
use crate::Translator;
use bimap::BiMap;
use serde::Deserialize;

/// A simple `Translator` implementation, uses `BiMap` under the hood.
/// `to_braille` returns `0` if string cannot be found in `charmap`.
/// `to_string` returns an empty string if braille character cannot be found in `charmap`.
#[derive(Deserialize, Debug)]
pub struct BrailleCharMap {
    charmap: BiMap<String, u8>,
}

impl BrailleCharMap {
    pub fn new(charmap: BiMap<String, u8>) -> Self {
        Self { charmap }
    }
}

impl Translator for BrailleCharMap {
    fn to_braille(&self, c: &String) -> u8 {
        self.charmap.get_by_left(c).cloned().unwrap_or(0)
    }

    fn to_string(&self, b: &u8) -> String {
        self.charmap
            .get_by_right(b)
            .cloned()
            .unwrap_or(String::from(""))
    }
}
