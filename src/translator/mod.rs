//! Translator trait and implementations.
pub mod simple;

pub use simple::BrailleCharMap;

/// Implements methods to convert to and from braille.
pub trait Translator {
    /// Finds matching braille character for given string.
    fn to_braille(&self, c: &String) -> u8;
    /// Finds matching string for given braille character.
    fn to_string(&self, b: &u8) -> String;
}
